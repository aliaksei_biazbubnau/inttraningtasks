package epamlab.anttask.customtask;


import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.*;
import java.io.File;
import java.util.*;

/**
 * Created by Aliaksei Biazbubnau on 06.06.2016.
 */
public class BuildFileValidator extends Task {
    private boolean checkDepends;
    private boolean checkDefault;
    private boolean checkNames;
    private Project project;
    private List<BuildFile> buildFiles = new ArrayList<BuildFile>();

    private static final String NAME_PATTERN = "[A-Za-z-]+";
    private static final String MAIN_NAME = "main";


    /**
     * Mutator method for the class field
     *
     * @param project value
     */
    public void setProject(Project project) {
        this.project = project;
    }

    /**
     * Mutator method for the class field
     *
     * @param checkDepends value
     */
    public void setCheckDepends(boolean checkDepends) {
        this.checkDepends = checkDepends;
    }

    /**
     * Mutator method for the class field
     *
     * @param checkDefault value
     */
    public void setCheckDefault(boolean checkDefault) {
        this.checkDefault = checkDefault;
    }

    /**
     * Mutator method for the class field
     *
     * @param checkNames value
     */
    public void setCheckNames(boolean checkNames) {
        this.checkNames = checkNames;
    }

    /**
     * Creates object and put it to collection of objects
     *
     * @return BuildFile object
     */
    public BuildFile createBuildFile() {
        BuildFile buildFile = new BuildFile();
        buildFiles.add(buildFile);
        return buildFile;
    }

    /**
     * Executes logical sequence of validation process
     */
    @Override
    public void execute() {

        for (BuildFile buildFile : buildFiles) {
            project.log("Validating " + buildFile.getPath() + "...");
            Project newProject = getNewProject(buildFile.getPath());
            if (checkNames) {
                namesValidation(newProject);
            }
            if (checkDefault) {
                defaultTargetValidation(newProject);
            }
            if (checkDepends) {
                dependenciesValidator(newProject);
            }

        }
    }

    /**
     * Create and configure new project with required parameters
     * @param path -path to the analizing buildfile
     * @return project - project object
     */
    private Project getNewProject(String path) {
        File file = new File(path);
        Project project = new Project();
        ProjectHelper.configureProject(project, file);
        return project;
    }
    /**
     * Validate target names according correct name requirements
     * @trows BuilException when name is invalid
     * @param newProject - validating project
     */
    private void namesValidation(final Project newProject) {
        project.log("Names validation...");
        Collection<Target> targets = newProject.getTargets().values();
        for (Target target : targets) {
            if (target.getName().isEmpty()) {
                continue;
            }
            if (!(target.getName().matches(NAME_PATTERN))) {
                throw new BuildException(target.getName() + " is invalid!");
            }
        }
        project.log("Names validation is done successfully.");
    }

    /**
     * Check project for existance of default target
     *
     *@throws BuildException when project have no default target
     * @param newProject - analizing Project
     */
    private void defaultTargetValidation(Project newProject) {
        project.log("Default target validation...");
        if (newProject.getDefaultTarget() == null) {
            throw new BuildException("There isn't default target in project!");
        } else {
            project.log("Default target validation is done successfully.");

        }
    }

    /**
     * Check project for absence main target as dependent
     *
     * @param newProject -analizing project
     */
    private void dependenciesValidator(Project newProject){
        {
            project.log("Dependencies validation...");
            Hashtable<String, Target> targets = newProject.getTargets();

            for (Target target : targets.values()) {
                if (target.getName().equals(MAIN_NAME)) {
                    continue;
                }
                if (target.getDependencies().hasMoreElements()) {
                    throw new BuildException(String.format("%s target can't contain dependencies!",
                            MAIN_NAME));
                }
            }
            project.log("Dependencies validation is done successfully");
        }


    }

    /**
     * Inner class that used for object creation that describe bild file object
     */

    public class BuildFile {
        private String path;

        /**
         * Default constructor.
         */
        public BuildFile() { }

        /**
         * Returns path.
         *
         * @return  path
         */
        public String getPath() {
            return path;
        }

        /**
         * Set the path.
         *
         * @param path
         */
        public void setPath(final String path) {
            this.path = path;
        }
    }
}